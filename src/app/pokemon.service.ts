import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

export interface PokemonType {
    id: number;
    name: string;
    url?: string;
    sprites?: any[string];
    stats?: any[];
}

export interface PokeApiList {
    count: number,
    next: string,
    prev: string,
    results: any[]
}


@Injectable({
    providedIn: 'root'
})
export class PokemonService {

    constructor(private http: HttpClient) { }

    private _pokemons: BehaviorSubject<PokemonType[]> = new BehaviorSubject<PokemonType[]>([]);
    readonly pokemons: Observable<PokemonType[]> = this._pokemons.asObservable();
    private pokeBall: { pokemons: PokemonType[] } = { pokemons: [] };

    getAllPokemons() : void {
        this.http.get<PokeApiList>('/api/v2/pokemon/').subscribe(d => {
            let count = d.count;

            this.http.get<PokeApiList>(`/api/v2/pokemon/?offset=0&limit=${count}`).subscribe(data => {
                let tempPokemon : PokemonType[] = []; 
                data.results.forEach(element => {
                    tempPokemon.push({
                        "name": element.name,
                        "url": element.url.replace('https://pokeapi.co',''),
                        "id": element.url.replace('https://pokeapi.co/api/v2/pokemon/','').slice(0, -1),
                    });
                });

                this.pokeBall.pokemons = tempPokemon;
                this._pokemons.next(Object.assign({}, this.pokeBall).pokemons);
            });
        });
    }

    getPokemonInfo(id: string | number) {
        return this.http.get<PokemonType>(`/api/v2/pokemon/${id}`);
    }
}