import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { PokemonService, PokemonType } from '../pokemon.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-pokemon',
    templateUrl: './pokemon.component.html',
    styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {

    constructor(private route: ActivatedRoute, public _pokemonService: PokemonService) { }

    pokemon: PokemonType = {
        name: '',
        id: 0,
        sprites: '',
        stats: []
    };

    next: PokemonType = {
        name: '',
        id: 0,
        sprites: '',
        stats: []
    };

    prev: PokemonType = {
        name: '',
        id: 0,
        sprites: '',
        stats: []
    };

    id: number;

    pokemons: Observable<PokemonType[]>;

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.id = parseInt(params.get("id"));
            this.pokemons = this._pokemonService.pokemons;

            this.pokemons.subscribe(data => {
                let index = data.findIndex(x => x.id == this.id);

                if (index !== -1) {
                    let nextId = index !== data.length ? index + 1 : 1;
                    let prevId = index !== 0 ? index - 1 : data.length;

                    this.next.id = data[nextId].id;
                    this.prev.id = data[prevId].id;

                    this.next.name = data[nextId].name;
                    this.prev.name = data[prevId].name;
                }
            });
            this._pokemonService.getPokemonInfo(this.id).subscribe(data => {
                this.pokemon = {
                    name: data.name,
                    sprites: data.sprites.front_default,
                    stats: data.stats,
                    id: data.id
                };
            });
        });
    }

    getPokemonId(text: string): number {
        return 0;
    }
}
