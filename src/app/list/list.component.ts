import { Component, OnInit } from '@angular/core';
import { PokemonService, PokemonType } from '../pokemon.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
    pokemons: Observable<PokemonType[]>;
    pokemonsList: PokemonType[] = [];

    stop: number = 10;
    start: number = 0;
    showOnPage: number = 10;

    paginationActiveIndex: number = 0;

    constructor(public _pokemonService: PokemonService) { }

    pagination(p: number) {
        console.log(p);
        this.start = p * this.showOnPage;
        this.stop = this.start + this.showOnPage;
    }

    ngOnInit() {
        this.pokemons = this._pokemonService.pokemons;
        this.pokemons.subscribe(data => {
            this.pokemonsList = data;
        });
    }

}
