import { Component } from '@angular/core';
import { PokemonService, PokemonType } from './pokemon.service';
import { Observable } from 'rxjs';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    pokemons: Observable<PokemonType[]>;
    pokemonsList: PokemonType[] = [];
    search: string = '';
    liveSearch: PokemonType[] = [];

    constructor(public _pokemonService: PokemonService) {

    }

    ngOnInit() {
        this.pokemons = this._pokemonService.pokemons;
        this._pokemonService.getAllPokemons();

        this.pokemons.subscribe(data => {
            this.pokemonsList = data;
        })
    }

    searchAction(e) {
        let tempL = this.liveSearch = [],
            regex = new RegExp(e, "i");

        for (let i = 0; i < this.pokemonsList.length; i++) {
            let el = this.pokemonsList[i];

            if (el.name.search(regex) != -1) {
                tempL.push(el);
            }
        }
        this.liveSearch = e == '' || e.length < 2 ? [] : tempL;
    }

}
